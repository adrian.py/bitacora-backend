FactoryBot.define do
  factory :marital_status do
    name { Faker::Name.name }
  end
end
