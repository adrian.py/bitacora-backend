FactoryBot.define do
  factory :medical_indicator do
    name { Faker::Lorem.words(number: 2) }
    mesure_unit { Faker::Lorem.word }
  end
end
