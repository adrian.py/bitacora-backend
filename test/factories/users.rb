FactoryBot.define do
  factory :user do
    full_name { Faker::Name.name }
    birth_date { Faker::Date.between(from: '2014-09-23', to: '2014-09-25') }
    address { Faker::Address.city }
    marital_status
    religion { Faker::Name.name }
    blood_type
    allergies { Faker::Lorem.sentence }
    medical_conditions { Faker::Lorem.sentence }
    sex
    email { Faker::Internet.email }
    password { Faker::Internet.password }
  end
end
