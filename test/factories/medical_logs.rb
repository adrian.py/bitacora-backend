FactoryBot.define do
  factory :medical_log do
    my_indicator
    value { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
  end
end
