FactoryBot.define do
  factory :blood_type do
    name { Faker::Blood.group }
  end
end
