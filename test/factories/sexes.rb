FactoryBot.define do
  factory :sex do
    name { Faker::Name.name }
  end
end
