# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_09_22_004343) do
  create_table "blood_types", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marital_statuses", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_indicators", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.string "mesure_unit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_logs", charset: "utf8mb4", force: :cascade do |t|
    t.bigint "my_indicator_id", null: false
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["my_indicator_id"], name: "index_medical_logs_on_my_indicator_id"
  end

  create_table "my_indicators", charset: "utf8mb4", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "medical_indicator_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["medical_indicator_id"], name: "index_my_indicators_on_medical_indicator_id"
    t.index ["user_id"], name: "index_my_indicators_on_user_id"
  end

  create_table "sexes", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", charset: "utf8mb4", force: :cascade do |t|
    t.string "full_name"
    t.date "birth_date"
    t.string "address"
    t.bigint "marital_status_id", null: false
    t.string "religion"
    t.bigint "blood_type_id", null: false
    t.string "allergies"
    t.string "medical_conditions"
    t.bigint "sex_id", null: false
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blood_type_id"], name: "index_users_on_blood_type_id"
    t.index ["marital_status_id"], name: "index_users_on_marital_status_id"
    t.index ["sex_id"], name: "index_users_on_sex_id"
  end

  add_foreign_key "medical_logs", "my_indicators"
  add_foreign_key "my_indicators", "medical_indicators"
  add_foreign_key "my_indicators", "users"
  add_foreign_key "users", "blood_types"
  add_foreign_key "users", "marital_statuses"
  add_foreign_key "users", "sexes"
end
