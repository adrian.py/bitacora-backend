# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

MaritalStatus.destroy_all
MaritalStatus.create!([
	{name: 'Soltero/a'},
	{name: 'Casado/a'},
	{name: 'Divorciado/a'},
	{name: 'Viudo/a'}
])
p "Created #{MaritalStatus.count} marital statuses"

BloodType.destroy_all
BloodType.create!([
	{name: 'A+'},
	{name: 'A-'},
	{name: 'B+'},
	{name: 'B-'},
	{name: 'AB+'},
	{name: 'AB-'},
	{name: 'O+'},
	{name: 'O-'},
])
p "Created #{BloodType.count} bloond types"

Sex.destroy_all
Sex.create!([
	{name: 'Masculino'},
	{name: 'Femenino'},
])
p "Created #{Sex.count} sexes"

MedicalIndicator.destroy_all
MedicalIndicator.create!([
	{name: 'Peso', mesure_unit: 'Kg'},
	{name: 'Talla', mesure_unit: 'cm'},
	{name: 'Temperatura', mesure_unit: 'ºC'},
	{name: 'Tensión Arterial', mesure_unit: 'mm Hg'},
	{name: 'Glucosa', mesure_unit: 'mg/dL'},
	{name: 'Urea', mesure_unit: 'mg/dL'},
	{name: 'Creatinina', mesure_unit: 'mg/dL'},
	{name: 'BUN', mesure_unit: 'mg/dL'},
	{name: 'BUN/CREAT', mesure_unit: 'mg/dL'},
	{name: 'Ácido Úrico', mesure_unit: 'mg/dL'},
	{name: 'Colesterol Total', mesure_unit: 'mg/dL'},
	{name: 'Trigliceridos', mesure_unit: 'mg/dL'},
	{name: 'Saturación Óxigeno', mesure_unit: 'PR Bpm'},
	{name: 'Frecuencia Cardiaca', mesure_unit: 'p/m'},
	{name: 'Frecuencia Respiratoria', mesure_unit: 'p/m'},
	{name: 'Ritmo menstrual', mesure_unit: 'días'},
	{name: 'Hemoglobina', mesure_unit: 'g/dL'},
	{name: 'Hematocrito', mesure_unit: '%'},
	{name: 'Eritrocitos', mesure_unit: 'x 10^6/uL'},
	{name: 'Leucocitos', mesure_unit: 'uL'},
	{name: 'VCM', mesure_unit: 'fL'},
	{name: 'HCM', mesure_unit: 'pg'},
	{name: 'CMHC', mesure_unit: 'g/dL'},
	{name: 'Linfocitos', mesure_unit: '%'},
	{name: 'Monocitos', mesure_unit: '%'},
	{name: 'Eosinofilos', mesure_unit: '%'},
	{name: 'Basofilos', mesure_unit: '%'},
	{name: 'Segmentados', mesure_unit: '%'},
	{name: 'Bandas', mesure_unit: '%'},
	{name: 'Linfocitos absolutos', mesure_unit: 'uL'},
	{name: 'Monocitos absolutos', mesure_unit: 'uL'},
	{name: 'Eosinofilos absolutos', mesure_unit: 'uL'},
	{name: 'Basofilos absolutos', mesure_unit: 'uL'},
	{name: 'Segmentados absolutos', mesure_unit: 'uL'},
	{name: 'Bandas absolutas', mesure_unit: 'uL'},
	{name: 'Plaquetas', mesure_unit: 'K/uL'}
])
p "Created #{MedicalIndicator.count} medical indicarors"