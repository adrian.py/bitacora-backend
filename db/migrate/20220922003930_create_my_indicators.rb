class CreateMyIndicators < ActiveRecord::Migration[7.0]
  def change
    create_table :my_indicators do |t|
      t.references :user, null: false, foreign_key: true
      t.references :medical_indicator, null: false, foreign_key: true

      t.timestamps
    end
  end
end
