class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :full_name
      t.date :birth_date
      t.string :address
      t.references :marital_status, null: false, foreign_key: true
      t.string :religion
      t.references :blood_type, null: false, foreign_key: true
      t.string :allergies
      t.string :medical_conditions
      t.references :sex, null: false, foreign_key: true
      t.string :email
      t.string :password_digest

      t.timestamps
    end
  end
end
