class CreateMedicalLogs < ActiveRecord::Migration[7.0]
  def change
    create_table :medical_logs do |t|
      t.references :my_indicator, null: false, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
