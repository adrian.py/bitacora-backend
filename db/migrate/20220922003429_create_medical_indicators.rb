class CreateMedicalIndicators < ActiveRecord::Migration[7.0]
  def change
    create_table :medical_indicators do |t|
      t.string :name
      t.string :mesure_unit

      t.timestamps
    end
  end
end
