require 'rails_helper'

RSpec.describe MedicalLog, type: :model do
  
	describe "validations" do
  	it "validate required fields" do
  		should validate_presence_of(:my_indicator)
  		should validate_presence_of(:value)
  	end
  end

  describe "validations relation myindicator" do
    it { should belong_to(:my_indicator) }
  end

end
