require 'rails_helper'

RSpec.describe BloodType, type: :model do

  describe "validations" do
  	it "validate presence of name" do
  		should validate_presence_of(:name)
  	end
  end

  describe "validations relation user" do
  	it { should have_many(:users) }
  end

end