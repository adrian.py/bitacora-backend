require 'rails_helper'

RSpec.describe User, type: :model do
  describe "validations" do
  	it "validate required fields" do
  		should validate_presence_of(:full_name)
  		should validate_presence_of(:birth_date)
  		should validate_presence_of(:address)
  		should validate_presence_of(:marital_status_id)
  		should validate_presence_of(:religion)
  		should validate_presence_of(:blood_type_id)
  		should validate_presence_of(:allergies)
  		should validate_presence_of(:medical_conditions)
  		should validate_presence_of(:sex_id)
  		should validate_presence_of(:email)
  		should validate_presence_of(:password)
  	end
  end

  describe "validations relation marital_status" do
    it { should belong_to(:marital_status) }
  end

  describe "validations relation blood_type" do
    it { should belong_to(:blood_type) }
  end

  describe "validations relation sex" do
    it { should belong_to(:sex) }
  end

  describe "validations relation my_indicator" do
    it { should have_many(:my_indicator) }
  end

end
