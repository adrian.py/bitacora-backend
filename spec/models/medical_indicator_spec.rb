require 'rails_helper'

RSpec.describe MedicalIndicator, type: :model do
  describe "validations" do
  	it "validate presence of name" do
  		should validate_presence_of(:name)
  		should validate_presence_of(:mesure_unit)
  	end
  end

end
