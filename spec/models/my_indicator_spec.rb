require 'rails_helper'

RSpec.describe MyIndicator, type: :model do
  
	describe "validations" do
  	it "validate presence of name && medical_indicator" do
  		should validate_presence_of(:user)
  		should validate_presence_of(:medical_indicator)
  	end
  end

  describe "validate relation user" do
  	it { should belong_to(:user) }
  end

  describe "validate relation medical_indicator" do
  	it { should belong_to(:medical_indicator) }
  end

end
