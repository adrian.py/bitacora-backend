require "rails_helper"

RSpec.describe "Users", type: :request do
	#Index
	describe "GET /users" do
		before { get '/users' }
		it "should return Ok" do
			payload = JSON.parse(response.body)
			expect(payload).to be_empty
			expect(response).to have_http_status(200)
		end
	end
	
	#Store
	describe "create user" do
		let!(:users) { create_list(:user, 10) }
		before { get '/users' }
		it "should return all users" do
			payload = JSON.parse(response.body)
			expect(payload.size).to eq(users.size)
			expect(response).to have_http_status(200)
		end
	end

	describe "POST /create User" do
  	let!(:marital_statuses) { create_list(:marital_status, 5) }  
    let!(:blood_types) { create_list(:blood_type, 10) }
    let!(:sexes) { create_list(:sex, 10) }


    it "should create user" do
    	req_payload = {
    		user: {
    		full_name: 'Leobardo Herrera',
				birth_date: '1991/11/18',
				address: 'Conocido',
				marital_status_id: rand(MaritalStatus.minimum('id').. MaritalStatus.maximum('id')),
				religion: "?",
				blood_type_id: rand(BloodType.minimum('id').. BloodType.maximum('id')),
				allergies: "Penicilina",
				medical_conditions: "Ninguna",
				sex_id: rand(Sex.minimum('id').. Sex.maximum('id')),
				email: "adrian129951@gmail.com",
				password: "mclaren"
    		}
    	}
    	#POST http
    	post "/users", params: req_payload
    	payload = JSON.parse(response.body)
    	expect(payload).to_not be_empty
    	expect(payload['id']).to_not be_nil
		expect(response).to have_http_status(:created)
    end

    it "should create return message error on users" do
      req_payload = {
        user: {
    			full_name: 'Leobardo Herrera',
				  birth_date: '1991/11/18'
    		}
      }
      #POST http
      post "/users", params: req_payload
      payload = JSON.parse(response.body)
      expect(payload).to_not be_empty
      expect(payload['error']).to_not be_empty
      expect(response).to have_http_status(:unprocessable_entity)
    end

  end

  #Update
  describe "PUT update/{id}" do
  	let!(:user) { create(:user) }
    let!(:marital_statuses) { create_list(:marital_status, 5) }
    let!(:blood_types) { create_list(:blood_type, 10) }
    let!(:sexes) { create_list(:sex, 10) }
  	it "should update user" do
  		req_payload = {
    		user: {
    		full_name: 'Leobardo Herrera',
				birth_date: '1991/11/18',
				address: 'Conocido',
				marital_status_id: rand(MaritalStatus.minimum('id').. MaritalStatus.maximum('id')),
				religion: "?",
				blood_type_id: rand(BloodType.minimum('id').. BloodType.maximum('id')),
				allergies: "Penicilina",
				medical_conditions: "Ninguna",
				sex_id: rand(Sex.minimum('id').. Sex.maximum('id'))
    		}
    	}
    	put "/users/#{user.id}", params: req_payload
    	payload = JSON.parse(response.body)
    	expect(payload).to_not be_empty
    	expect(payload['id']).to eq(user.id)
    	expect(payload['full_name']).to eq('Leobardo Herrera')
		expect(response).to have_http_status(:ok)
  	end

    it "should update return message error on user" do
      req_payload = {
        user: {
    		full_name: nil,
				birth_date: nil,
				address: nil
    		}
      }
      put "/users/#{user.id}", params: req_payload
      payload = JSON.parse(response.body)
      expect(payload).to_not be_nil
      expect(payload['error']).to_not be_empty
      expect(response).to have_http_status(:unprocessable_entity)
    end

  end



end