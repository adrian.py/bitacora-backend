require 'rails_helper'

RSpec.describe "MedicalIndicators", type: :request do
	describe "GET /index" do
    it "returns http success" do
      get "/medical_indicators"
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST /create" do
    
    it "should create medical_indicator" do
    	req_payload = {
    		medical_indicator: {
    			name: "T/A",
				  mesure_unit: "mm Hg"
    		}
    	}
    	#POST http
    	post "/medical_indicators", params: req_payload
    	payload = JSON.parse(response.body)
    	expect(payload).to_not be_empty
    	expect(payload['id']).to_not be_nil
		  expect(response).to have_http_status(:created)
    end

    it "should create return message error on medical_indicator" do
      req_payload = {
        medical_indicator: {
          mesure_unit: "mm Hg"
        }
      }
      #POST http
      post "/medical_indicators", params: req_payload
      payload = JSON.parse(response.body)
      expect(payload).to_not be_empty
      expect(payload['error']).to_not be_empty
      expect(response).to have_http_status(:unprocessable_entity)
    end

  end

  describe "PUT update/{id}" do
  	let!(:indicator) { create(:medical_indicator) }
  	it "should update medical_indicator" do
  		req_payload = {
    		medical_indicator: {
    			name: "Tensión Arterial",
				  mesure_unit: "mm Hg"
    		}
    	}
    	put "/medical_indicators/#{indicator.id}", params: req_payload
    	payload = JSON.parse(response.body)
    	expect(payload).to_not be_empty
    	expect(payload['id']).to eq(indicator.id)
    	expect(payload['name']).to eq('Tensión Arterial')
		  expect(response).to have_http_status(:ok)
  	end

    it "should update return message error on  medical_indicator" do
      req_payload = {
        medical_indicator: {
          name: nil,
          mesure_unit: nil
        }
      }
      put "/medical_indicators/#{indicator.id}", params: req_payload
      payload = JSON.parse(response.body)
      expect(payload).to_not be_nil
      expect(payload['error']).to_not be_empty
      expect(response).to have_http_status(:unprocessable_entity)
    end

  end

end
