require 'rails_helper'

RSpec.describe "BloodTypes", type: :request do
	describe "GET /index" do
    it "returns http success" do
      get "/blood_types"
      expect(response).to have_http_status(:success)
    end
  end
end
