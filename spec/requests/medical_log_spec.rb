require 'rails_helper'

RSpec.describe "Medical Logs", type: :request do
  
  #Index
  describe "GET /medical_log/{id}" do
  		let!(:user) { create(:user) }
		before { get "/medical_log/#{user.id}" }
		it "should return Ok" do
			payload = JSON.parse(response.body)
			expect(payload).to be_empty
			expect(response).to have_http_status(200)
		end
	end


end