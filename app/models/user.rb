class User < ApplicationRecord
  has_secure_password

  belongs_to :marital_status
  belongs_to :blood_type
  belongs_to :sex
  has_many :my_indicator

  validates :full_name, presence: true
  validates :birth_date, presence: true
  validates :address, presence: true
  validates :marital_status_id, presence: true
  validates :religion, presence: true
  validates :blood_type_id, presence: true
  validates :allergies, presence: true
  validates :medical_conditions, presence: true
  validates :sex_id, presence: true
  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  #validates :password

end