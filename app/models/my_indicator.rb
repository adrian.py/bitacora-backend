class MyIndicator < ApplicationRecord
  belongs_to :user
  belongs_to :medical_indicator

  validates :user, presence: true
  validates :medical_indicator, presence: true
end
