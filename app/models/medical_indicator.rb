class MedicalIndicator < ApplicationRecord
	has_many :my_indicator

	validates :name, presence: true 
	validates :mesure_unit, presence: true
end
