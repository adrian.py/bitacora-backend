class MedicalLog < ApplicationRecord
  belongs_to :my_indicator

  validates :my_indicator, presence: true
  validates :value, presence: true
end
