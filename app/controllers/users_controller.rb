class UsersController < ApplicationController

  before_action :authorize_request, except: :create
  before_action :find_user, except: %i[create index]

	#rescue_from Exception do |e|
    #log.error "#{e.message}"
    #render json: {error: e.message}, status: :internal_error
  #end

  #Mayor prioridad que el que esta antes
  rescue_from ActiveRecord::RecordInvalid do |e|
    render json: {error: e.message}, status: :unprocessable_entity
  end

  def index
  	@users = User.all
  	render json: @users, status: :ok
  end
  
  def create
  	@user = User.create!(create_params)
  	render json: @user, status: :created
  end

  def update
  	@user = User.find(params[:id])
  	@user.update!(update_params)
  	render json: @user, status: :ok
  end

  def find_user
    @user = User.find_by_full_name!(params[:fulla_name])
    rescue ActiveRecord::RecordNotFound
      render json: { errors: 'User not found' }, status: :not_found
  end

  private

  def create_params
  	params.require(:user).permit(:full_name, :birth_date, :address, :marital_status_id, :religion, :blood_type_id, :allergies, :medical_conditions, :sex_id, :email, :password)
  end

  def update_params
  	params.require(:user).permit(:full_name, :birth_date, :address, :marital_status_id, :religion, :blood_type_id, :allergies, :medical_conditions, :sex_id)
  end

end
