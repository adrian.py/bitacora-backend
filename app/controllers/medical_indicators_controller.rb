class MedicalIndicatorsController < ApplicationController

  rescue_from Exception do |e|
    #log.error "#{e.message}"
    render json: {error: e.message}, status: :internal_error
  end

  #Mayor prioridad que el que esta antes
  rescue_from ActiveRecord::RecordInvalid do |e|
    render json: {error: e.message}, status: :unprocessable_entity
  end

  def index
    @indicators = MedicalIndicator.select('id', 'name', 'mesure_unit')
    render json: @indicators, status: :ok
  end

  # POST /medical_indicators
  def create
  	@indicator = MedicalIndicator.create!(create_params)
  	render json: @indicator, status: :created
  end

  # PUT /medical_indicators/{id}
  def update
  	@indicator = MedicalIndicator.find(params[:id])
  	@indicator.update!(update_params)
  	render json: @indicator, status: :ok
  end

  private 

  def create_params
  	params.require(:medical_indicator).permit(:name, :mesure_unit)
  end

  def update_params
  	params.require(:medical_indicator).permit(:name, :mesure_unit)
  end


end
