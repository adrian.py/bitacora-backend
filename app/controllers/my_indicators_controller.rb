class MyIndicatorsController < ApplicationController
	before_action :authenticate_user!, only: [:show, :create, :destroy]

	rescue_from Exception do |e|
		logger.debug "#{e.message}"
		render json: {error: e.message}, status: :internal_error
	end

	rescue_from ActiveRecord::RecordInvalid do |e|
		render json: {error: e.message}, status: :unprocessable_entity
	end


	def show
		@indicators = Current.user.my_indicator.where("users.id = "+params[:id])
		render json: @indicators.includes(:user, :medical_indicator), status: :ok
	end

	def create
		@indicator = MyIndicator.create!(create_params)
    	render json: @log, status: :created
	end

	def destroy
		MyIndicator.where("users.id = "+params[:id]).destroy_all
	end

	def create_params
		params.require(:my_indicator).permit(:user_id, :medical_indicator_id)
	end

	#def authenticate_user!
#		#Bearer XXXX--XXXX
#		token_regex = /Bearer (\w+)/
#		#Leer Header de auth
#		headers = request.headers
#		#Verificar que sea valido
#		if headers['Authorization'].present? && headers['Authorization'].match(token_regex)
#			token = headers['Authorization'].match(token_regex)[1]
#			#Verificar que el token correspoda a un usuario
#			if(Current.user = User.find_by_auth_token(token))#

#				endreturn 
#		end
#		render json: {error: "Unauthorized"}, status: :unauthorized
#	end

end
