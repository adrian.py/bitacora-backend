class MedicalLogController < ApplicationController
 
  rescue_from Exception do |e|
    logger.debug "#{e.message}"
    render json: {error: e.message}, status: :internal_error
  end

  #Mayor prioridad que el que esta antes
  rescue_from ActiveRecord::RecordInvalid do |e|
    render json: {error: e.message}, status: :unprocessable_entity
  end
 
  def show
  	@log = MedicalLog.joins(my_indicator: [:user]).where("users.id = "+params[:id])
  	render json: @log, status: :ok
  end
  
  def create
    @log = MedicalLog.create!(create_params)
    render json: @log, status: :created
  end
  
  def update
    @log = MedicalLog.find(params[:id])
    @log.update!(update_params)
    render json: @log, status: :ok
  end

  def create_params
    params.require(:medical_log).permit(:my_indicator, :value)
  end

  def update_params
    params.require(:medical_log).permit(:value)
  end
end
