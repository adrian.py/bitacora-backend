class SexesController < ApplicationController
  def index
  	@sexes = Sex.select('id', 'name')
  	render json: @sexes, status: :ok
  end
end #Complete
