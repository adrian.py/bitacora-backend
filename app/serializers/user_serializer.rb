class UserSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :birth_date, :address, :marital_status, :religion, :blood_type, :allergies, :medical_conditions, :sex, :email

  #Relationship for sex
  def sex 
  	sex = self.object.sex
  	{
  		id: sex.id,
  		name: sex.name
  	}
  end
  
  #Relationship for marital status
  def marital_status
  	marital_status = self.object.marital_status
  	{
  		id: marital_status.id,
  		name: marital_status.name
  	}
  end

  #Relationship for blood type
  def blood_type
  	blood_type = self.object.blood_type
  	{
  		id: blood_type.id,
  		name: blood_type.name
  	}
  end 

end
