class MedicalIndicatorSerializer < ActiveModel::Serializer
  attributes :id, :name, :mesure_unit #Example , author

  #Example
  #def author 
  #	user = self.object.user
  #	{
  #		id: user.id,
  #		name: user.name,
  #		email: user.email
  #	}
  #end
end
