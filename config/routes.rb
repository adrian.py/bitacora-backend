Rails.application.routes.draw do  
  get 'my_indicators/show'
  get 'my_indicators/create'
  get 'my_indicators/destroy'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  resources :health, only: [:index]
  resources :medical_indicators, only: [:index, :create, :update]
  resources :marital_statuses, only: [:index]
  resources :blood_types, only: [:index]
  resources :sexes, only: [:index]
  resources :users, only: [:index, :create, :update]
  resources :medical_log, only: [:show, :create, :update]

  post '/auth/login', to: 'authentication#login'

  get '/*a', to: 'application#not_found'
end
